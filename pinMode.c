/*! \file  pinMode.c
 *
 *  \brief Set a digital pin to be input or output
 *
 *
 *  \author jjmcd
 *  \date March 19, 2013, 9:42 AM
 *
 * Software License Agreement
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 */

#include "Arduino.h"

/*! Array of pin definitions in order of pin number */
/*! This array maps the information needed to set/reset a bit
 *  into a single long that may be passed to a function.  For
 *  each element of the array, the low word is the address of
 *  the LAT register, the high word a mask with the associated
 *  bit turned off.  From these values the various macros in
 *  Arduino.h can calculate a LAT, PORT or TRIS bit.
 * */
const long lArduinoPins[19] =
{ 
   (long)0xbfff02d0,
   (long)0xdfff02d0,
   (long)0xfeff02ca,
   (long)0xfffe02d6,
   (long)0xfeff02dc,
   (long)0xfffd02d6,
   (long)0xfffb02d6,
   (long)0xffdf02e2,
   (long)0xffdf02dc,
   (long)0xfff702d6,
   (long)0xffbf02e2,
   (long)0xfffb02e2,
   (long)0xfff702e2,
   (long)0xffef02dc,
   (long)0xfffe02ca,
   (long)0xfffd02ca,
   (long)0xfffb02ca,
   (long)0xfff702ca,
   (long)0xffef02ca
 };


/*! pinMode - Set a digital pin to be input or output */
/*! Set the TRIS bit for the appropriate pin
 */
void pinMode(int nPin, int nDirection )
{
    if ( nPin > 18 )
        return;
    if ( nDirection )
        *(TRISPTR( lArduinoPins[nPin] )) |= PINMASKR( lArduinoPins[nPin] );
    else
        *(TRISPTR( lArduinoPins[nPin] )) &= PINMASK( lArduinoPins[nPin] );
}
