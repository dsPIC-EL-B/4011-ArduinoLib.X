/*! \file  digitalRead.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date March 19, 2013, 10:20 AM
 *
 * Software License Agreement
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 */

#include "Arduino.h"

extern const long lArduinoPins[19];

/*! digitalRead - */
/*!
 *
 */
int digitalRead( int nPin )
{
    if ( nPin>18 )
        return 0;
    if ( *(PORTPTR( lArduinoPins[nPin] )) & PINMASKR( lArduinoPins[nPin] ) )
        return 1;
    else
        return 0;
    return -1;
}
