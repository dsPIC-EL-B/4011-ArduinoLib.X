/*! \file  analogRead.c
 *
 *  \brief Read an analog pin
 *
 *
 *  \author jjmcd
 *  \date March 19, 2013, 12:50 PM
 *
 * Software License Agreement
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 */

#include "Arduino.h"

/*! analogRead - Read an analog pin */
/*! Initializes the analog port, yest ,much of this is redundant if we
 *  are reading multiple pins or the same pin repeatedly.  Then start
 *  the conversion, wait for completion, and return the result.
 */
int analogRead( int nPin )
{
    /* Initialization not specific to channel */
    ADCON1 = 0x00E4;
    ADCON2 = 0x0000;
    ADCON3 = 0x1f3f;
    ADCSSL = 0;
    IFS0bits.ADIF = 0;
    IEC0bits.ADIE = 0;
    ADCON1bits.ADON = 1;

    /* Enable requested channel */
    ADPCFG = 0xffff;        /* First set all channels off */
    /* Now turn on the channel depending on selected pin */
    switch ( nPin )
    {
        case A0:
            ADPCFGbits.PCFG0 = 0;
            ADCHS = 0x2f20;
            break;
        case A1:
            ADPCFGbits.PCFG1 = 0;
            ADCHS = 0x2f21;
            break;
        case A2:
            ADPCFGbits.PCFG2 = 0;
            ADCHS = 0x2f22;
            break;
        case A3:
            ADPCFGbits.PCFG3 = 0;
            ADCHS = 0x2f23;
            break;
        case A4:
            ADPCFGbits.PCFG4 = 0;
            ADCHS = 0x2f24;
            break;
        case A5:
            ADPCFGbits.PCFG5 = 0;
            ADCHS = 0x2f25;
            break;
        default:
            ADPCFGbits.PCFG7 = 0;
            ADCHS = 0x2f26;
    }
    /* reset ADC interrupt flag */
    IFS0bits.ADIF = 0;
    // Wait for conversion
    while (!IFS0bits.ADIF);
    /* turn off ADC module */
    //ADCON1bits.ADON = 0;
    return ADCBUF0;
}

