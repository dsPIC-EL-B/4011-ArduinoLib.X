/*! \file  digitalWrite.c
 *
 *  \brief Set a digital pin
 *
 *
 *  \author jjmcd
 *  \date March 19, 2013, 9:42 AM
 *
 * Software License Agreement
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 */

#include "Arduino.h"

extern const long lArduinoPins[19];

/*! digitalWrite - Set a digital pin */
/*! Sets the state of a digital pin high or low.  Presumes pin has
 *  been previously made an output by pinMode().
 */
void digitalWrite( int nPin, int nState )
{
    if ( nPin > 18 )
        return;
    if ( nState )
        *(LATPTR( lArduinoPins[nPin] )) |= PINMASKR( lArduinoPins[nPin] );
    else
        *(LATPTR( lArduinoPins[nPin] )) &= PINMASK( lArduinoPins[nPin] );
}
