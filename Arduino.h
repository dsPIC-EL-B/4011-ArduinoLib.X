/*! \file  Arduino.h
 *
 *  \brief Arduino definitions for sketches
 *
 *
 *  \author jjmcd
 *  \date March 19, 2013, 9:40 AM
 *
 * Software License Agreement
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 */

#ifndef ARDUINO_H
#define	ARDUINO_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include <xc.h>
#include <libpic30.h>

/*! Instruction clock Hz */
#define Fcy  29491200

/*! Counts for a 1 ms delay */
#define Delay_1mS_Cnt     ((unsigned long)(Fcy * 0.001)-2)

/*! Delay a specified time */
#define delay(n) __delay32(((long)n*Delay_1mS_Cnt))

/* HIGH and LOW are defined for digitalRead() */
#define HIGH 1
#define LOW 0
    
/* INPUT and OUTPUT are defined for pinMode() */
#define INPUT 1
#define OUTPUT 0

/* Analog pins A0 through A5 are also digital pins 14 through 19 */
#define A0 14
#define A1 15
#define A2 16
#define A3 17
#define A4 18
#define A5 19

/* Define the mainline so it gets included */
int main ( int, char** );

/* Set the digital pin direction */
void pinMode(int, int ) ;
/* Set a digital pin high or low */
void digitalWrite( int, int );
/* Read the level of a digital pin */
int digitalRead( int );
/* Read the voltage on an analog pin */
int analogRead( int );
/* Set a PWM output */
void analogWrite( int, int );

/*! Return the mask part of the pin definition longword */
#define PINMASK(n) ((int)(n>>16))
/*! Return the complemented mask ffrom the pin definition longword*/
#define PINMASKR(n) (PINMASK(n)^0xffff)
/*! Return the address of the LAT register for the pin*/
#define LATPTR(n) ((int *)((int)(n & 0xffff)))
/*! Return the address of the TRIS register for the pin*/
#define TRISPTR(n) (LATPTR(n)-2)
/*! Return the address of the PORT register for the pin*/
#define PORTPTR(n) (LATPTR(n)-1)

/*! Pin definition longword for RB1 */
#define PIN_RB0  (long)(0xfffe0000L | (long)((int)&LATB))
#define PIN_RB1  (long)(0xfffd0000L | (long)((int)&LATB))
#define PIN_RB2  (long)(0xfffb0000L | (long)((int)&LATB))
#define PIN_RB3  (long)(0xfff70000L | (long)((int)&LATB))
#define PIN_RB4  (long)(0xffef0000L | (long)((int)&LATB))
#define PIN_RB5  (long)(0xffdf0000L | (long)((int)&LATB))
#define PIN_RB6  (long)(0xffbf0000L | (long)((int)&LATB))
#define PIN_RB7  (long)(0xff7f0000L | (long)((int)&LATB))
#define PIN_RB8  (long)(0xfeff0000L | (long)((int)&LATB))
#define PIN_RC13 (long)(0xff7b0000L)| (long)((int)&LATC))
#define PIN_RC14 (long)(0xff7d0000L)| (long)((int)&LATC))
#define PIN_RC15 (long)(0xff7f0000L)| (long)((int)&LATC))
#define PIN_RD0  (long)(0xfffe0000L | (long)((int)&LATD))
#define PIN_RD1  (long)(0xfffd0000L | (long)((int)&LATD))
#define PIN_RD2  (long)(0xfffb0000L | (long)((int)&LATD))
#define PIN_RD3  (long)(0xfff70000L | (long)((int)&LATD))
#define PIN_RE0  (long)(0xfffe0000L | (long)((int)&LATE))
#define PIN_RE1  (long)(0xfffd0000L | (long)((int)&LATE))
#define PIN_RE2  (long)(0xfffb0000L | (long)((int)&LATE))
#define PIN_RE3  (long)(0xfff70000L | (long)((int)&LATE))
#define PIN_RE4  (long)(0xffef0000L | (long)((int)&LATE))
#define PIN_RE5  (long)(0xffdf0000L | (long)((int)&LATE))
#define PIN_RE8  (long)(0xfeff0000L | (long)((int)&LATE))
#define PIN_RF0  (long)(0xfffe0000L | (long)((int)&LATF))
#define PIN_RF1  (long)(0xfffd0000L | (long)((int)&LATF))
#define PIN_RF2  (long)(0xfffb0000L | (long)((int)&LATF))
#define PIN_RF3  (long)(0xfff70000L | (long)((int)&LATF))
#define PIN_RF4  (long)(0xffef0000L | (long)((int)&LATF))
#define PIN_RF5  (long)(0xffdf0000L | (long)((int)&LATF))
#define PIN_RF6  (long)(0xffbf0000L | (long)((int)&LATF))

/*! Pin definition longword for shield pin 0 */
#define PIN_0    (long)(0xff7d0000L)| (long)((int)&LATC))
#define PIN_1    (long)(0xff7b0000L)| (long)((int)&LATC))
#define PIN_2    (long)(0xfeff0000L | (long)((int)&LATB))
#define PIN_3    (long)(0xfffe0000L | (long)((int)&LATD))
#define PIN_4    (long)(0xfeff0000L | (long)((int)&LATE))
#define PIN_5    (long)(0xfffd0000L | (long)((int)&LATD))
#define PIN_6    (long)(0xfffb0000L | (long)((int)&LATD))
#define PIN_7    (long)(0xffdf0000L | (long)((int)&LATF))
#define PIN_8    (long)(0xffdf0000L | (long)((int)&LATE))
#define PIN_9    (long)(0xfff70000L | (long)((int)&LATD))
#define PIN_10   (long)(0xffbf0000L | (long)((int)&LATF))
#define PIN_11   (long)(0xfff70000L | (long)((int)&LATF))
#define PIN_12   (long)(0xfffb0000L | (long)((int)&LATF))
#define PIN_13   (long)(0xffef0000L | (long)((int)&LATE))
#define PIN_14   (long)(0xfffe0000L | (long)((int)&LATB))
#define PIN_15   (long)(0xfffd0000L | (long)((int)&LATB))
#define PIN_16   (long)(0xfffb0000L | (long)((int)&LATB))
#define PIN_17   (long)(0xfff70000L | (long)((int)&LATB))
#define PIN_18   (long)(0xffef0000L | (long)((int)&LATB))
#define PIN_28   (long)(0xffef0000L | (long)((int)&LATF))
#define PIN_27   (long)(0xfffd0000L | (long)((int)&LATF))
#define PIN_29   (long)(0xfffe0000L | (long)((int)&LATF))
#define PIN_30   (long)(0xfff70000L | (long)((int)&LATE))
#define PIN_31   (long)(0xfffb0000L | (long)((int)&LATE))
#define PIN_32   (long)(0xfffd0000L | (long)((int)&LATE))
#define PIN_33   (long)(0xfffe0000L | (long)((int)&LATE))


#ifdef	__cplusplus
}
#endif

#endif	/* ARDUINO_H */

