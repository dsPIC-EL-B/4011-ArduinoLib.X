/*! \file  analogWrite.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date March 23, 2013, 5:00 PM
 *
 * Software License Agreement
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 */

#include <xc.h>



/*! analogWrite - */

/*!
 *
 */
void analogWrite( int nPin, int nValue )
{
    static int nTimerInit = 0;
    static int nInit[4] = { 0, 0, 0, 0 };
    const static int nPWMpins[4] = { 3,5,6,9 };
    int i;
    int nPWMnumber;

    nPWMnumber = -1;
    for ( i=0; i<4; i++ )
        if ( nPWMpins[i] == nPin )
            nPWMnumber = i;
    if ( nPWMnumber<0 )
        return;
    
    if ( !nTimerInit )
    {
        // Set up timer 2 for PWM
        TMR2 = 0;               // Clear timer 2
        PR2 = 255;             // Timer 2 counter to 1000
        T2CON = 0x8010;         // Fosc/4, 1:4 prescale, start TMR2
        nTimerInit = 1;
    }
    if ( !nInit[nPWMnumber] )
    {
        switch (nPWMnumber)
        {
            case 0:
                // Set up PWM on OC1 (RD0)
                //OC1RS = 1001;           // PWM 1 duty cycle - LED off
                OC1R = 0;               // Clear the PWM
                OC1CON = 0x6;           // Set OC1 to PWM mode, timer 2
                break;
            case 1:
                // Set up PWM on OC2 (RD1)
                //OC2RS = 1001;           // PWM 2 duty cycle
                OC2R = 0;               //
                OC2CON = 0x6;           // Set OC2 to PWM mode, timer 2
                break;
            case 2:
                // Set up PWM on OC3 (RD2)
                //OC3RS = 1001;           // PWM 3 duty cycle
                OC3R = 0;               //
                OC3CON = 0x6;           // Set OC3 to PWM mode, timer 2
                break;
            case 3:
                // Set up PWM on OC4 (RD3)
                //OC4RS = 1001;           // PWM 4 duty cycle
                OC4R = 0;               //
                OC4CON = 0x6;           // Set OC4 to PWM mode, timer 2
                break;
        }
        nInit[nPWMnumber] = 1;
    }
    if ( nValue>255 )
        nValue = 255;
    if ( nValue<0 )
        nValue = 0;
    switch (nPWMnumber)
    {
        case 0:
            OC1RS = nValue;
            break;
        case 1:
            OC2RS = nValue;
            break;
        case 2:
            OC3RS = nValue;
            break;
        case 3:
            OC4RS = nValue;
            break;
    }
}
